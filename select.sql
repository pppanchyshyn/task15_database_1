use labor_sql;
#Task1.1
select maker, type 
from product
order by maker;
#Task1.2
select model, ram, screen, price 
from laptop
where price > 1000
order by ram, price desc;
#Task1.3
select * from printer
where color = "y"
order by price desc;
#Task1.4
select model, speed, hd, cd, price 
from pc
where (cd = "12x" or "24x") and price < 600
order by speed;
#Task1.5
select name, class 
from ships
order by name;
#Task1.6
select * 
from pc
where speed >= 500 and price < 500
order by price desc; 
#Task1.7
select * 
from printer
where type != "Matrix" and price < 300
order by type desc;
#Task1.8
select model, speed, hd 
from pc
where price between 400 and 600
order by hd;
#Task1.9
select pc.model, speed, hd 
from pc join product on pc.model = product.model
where hd = 10 or hd = 20 and maker = "A"
order by speed;
#Task1.10
select model, speed, hd, price 
from laptop
where screen >= 12
order by price desc;
#Task1.11
select model, type, price 
from printer
where price < 300
order by type desc;
#Task2.1
select model
from pc
where model regexp '1{2,4}';
#Task2.2
select *
from outcome
where date regexp '-03-';
#Task2.3
select *
from outcome_o
where date regexp '-14 ';
#Task2.4
select name
from ships
where name regexp '^W.*n$';
#Task2.5
select name
from ships
where name regexp '^.*e.*e.*$';
#Task2.6
select name, launched
from ships
where name regexp '[^a]$';
#Task2.7
select name
from battles
where name regexp '^.* .*[^c]$';
#Task2.8
select *
from trip
where (time_out) between 12 and 17;
#Task2.12
select name
from passenger
where name regexp '^.* C.*$';
#Task2.12
select name
from passenger
where name regexp '^.* [^J].*$';
#Task3.1
select maker, type, speed, hd
from product join pc on product.model = pc.model
where hd <= 8;
#Task3.2
select maker
from product join pc on product.model = pc.model
where speed >= 600;
#Task3.3
select maker
from product join laptop on product.model = laptop.model
where speed <= 500;
#Task3.4
select distinct l1.model model1, l2.model model2, l1.hd, l1.ram
from laptop l1, laptop l2
where l1.hd = l2.hd and l1.model != l2.model;
#Task3.5
select c1.class class1, c2.class class2, c1.country, c1.type
from classes c1, classes c2
where c1.country = c2.country and c1.type = 'bb' and c2.type = 'bc' and c1.class != c2.class;
#Task3.6
select product.model, maker
from product join pc on product.model = pc.model
where price < 600;
#Task3.7
select product.model, maker
from product join printer on product.model = printer.model
where price > 300;
#Task3.8
select maker, pc.model, price
from product right join pc on product.model = pc.model;
#Task3.9
select maker, pc.model, price
from product right join pc on product.model = pc.model
where price is not null;
#Task3.10
select maker, type, laptop.model, speed
from product right join laptop on product.model = laptop.model
where speed > 600;
